## webscan with `Tor`

    Need one tor instance

    # apt install tor
    
    $ npm install
    or
    $ yarn

    $ node index http://www.congreso.es
    Found: http://www.congreso.es/robots.txt
    Found: http://www.congreso.es/favicon.ico
    Found: http://www.congreso.es/web/

## License

MIT