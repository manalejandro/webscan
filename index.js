const fs = require('fs'),
    request = require('request'),
    host = process.argv[2],
    Agent = host.match(/https.*/) ?
        require('socks5-https-client/lib/Agent') :
        require('socks5-http-client/lib/Agent')

fs.readFile(__dirname + '/path.txt', 'utf8', (err, data) => {
    if (err) console.log(err)
    else data.split('\n').map(path => {
        if (path.length > 0)
            request({
                url: host + path,
                method: 'GET',
                encoding: 'binary',
                headers: {
                    'User-Agent': 'Mozilla'
                },
                agentClass: Agent,
                agentOptions: {
                    socksPort: 9050
                }
            }, (error, response, body) => {
                if (error) console.log(error)
                else if (response.statusCode === 200)
                    console.log('Found: ' + host + path)
            })
    })
})
